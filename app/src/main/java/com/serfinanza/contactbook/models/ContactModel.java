package com.serfinanza.contactbook.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class ContactModel implements Parcelable {
    private String names;
    private String surname;
    private String phone;
    private String address;
    private String email;

    public ContactModel() {
        names = "";
        surname = "";
        phone = "";
        address = "";
        email = "";
    }

    protected ContactModel(Parcel in) {
        names = in.readString();
        surname = in.readString();
        phone = in.readString();
        address = in.readString();
        email = in.readString();
    }

    public static final Creator<ContactModel> CREATOR = new Creator<ContactModel>() {
        @Override
        public ContactModel createFromParcel(Parcel in) {
            return new ContactModel(in);
        }

        @Override
        public ContactModel[] newArray(int size) {
            return new ContactModel[size];
        }
    };

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(names);
        dest.writeString(surname);
        dest.writeString(phone);
        dest.writeString(address);
        dest.writeString(email);
    }
}


