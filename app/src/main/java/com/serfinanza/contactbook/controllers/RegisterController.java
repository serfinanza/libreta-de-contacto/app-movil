package com.serfinanza.contactbook.controllers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.serfinanza.contactbook.R;
import com.serfinanza.contactbook.models.ContactModel;

import java.util.ArrayList;
import java.util.Objects;

public class RegisterController extends AppCompatActivity {

    ArrayList<ContactModel> contacts;

    Toolbar mActionBarToolbar;
    EditText editTextName, editTextSurname, editTextPhone, editTextEmail, editTextAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getExtras();

        // toolbar
        mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        mActionBarToolbar.setTitle("Registrar contacto");
        setSupportActionBar(mActionBarToolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        editTextName = findViewById(R.id.editTextName);
        editTextSurname = findViewById(R.id.editTextSurname);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextAddress = findViewById(R.id.editTextAddress);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.register_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.itemSaveContact && validate()) {

            ContactModel model = new ContactModel();

            model.setNames(editTextName.getText().toString());
            model.setSurname(editTextSurname.getText().toString());
            model.setPhone(editTextPhone.getText().toString());
            model.setEmail(editTextEmail.getText().toString());
            model.setAddress(editTextAddress.getText().toString());

            contacts.add(model);

            goMain();

            return true;

        }

        return super.onOptionsItemSelected(item);

    }


    public boolean validate() {

        if (editTextName.getText().toString().length() <= 0) {
            showError("El nombre es requerido");
            editTextName.requestFocus();
            return false;
        }

        if (editTextSurname.getText().toString().length() <= 0) {
            showError("El apellido es requerido");
            editTextSurname.requestFocus();
            return false;
        }

        if (editTextPhone.getText().toString().length() <= 0) {
            showError("El celular es requerido");
            editTextPhone.requestFocus();
            return false;
        }

        if (editTextEmail.getText().toString().length() <= 0) {
            showError("El correo electronico es requerido");
            editTextEmail.requestFocus();
            return false;
        }

        if (editTextAddress.getText().toString().length() <= 0) {
            showError("La dirección es requerida");
            editTextAddress.requestFocus();
            return false;
        }

        return true;
    }

    private void showError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        contacts = bundle.getParcelableArrayList("contacts");
    }

    private void goMain() {
        Intent intent = new Intent(RegisterController.this, MainController.class);
        intent.putParcelableArrayListExtra("contacts", contacts);
        startActivity(intent);
    }


}