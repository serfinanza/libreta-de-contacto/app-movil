package com.serfinanza.contactbook.controllers;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.serfinanza.contactbook.R;
import com.serfinanza.contactbook.adapters.MainAdapter;
import com.serfinanza.contactbook.models.ContactModel;

public class ContactController extends AppCompatActivity {

    TextView textViewName, textViewPhone, textViewEmail, textViewAddress;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        textViewName = findViewById(R.id.textViewName);
        textViewPhone = findViewById(R.id.textViewPhone);
        textViewEmail = findViewById(R.id.textViewEmail);
        textViewAddress = findViewById(R.id.textViewAddress);
        mContext = this;
        getExtras();

    }


    private void getExtras() {

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {

            String name = bundle.getString("names");
            String surname = bundle.getString("surnames");
            String phone = bundle.getString("phone");
            String email = bundle.getString("email");
            String address = bundle.getString("address");

            textViewName.setText(String.format("%s %s", name, surname));
            textViewPhone.setText(phone);
            textViewEmail.setText(email);
            textViewAddress.setText(address);

        }

    }

    public void goCall(View v) {

        if (ActivityCompat.checkSelfPermission(ContactController.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + textViewPhone.getText().toString()));
            startActivity(intent);

        } else {

            ActivityCompat.requestPermissions(
                    ContactController.this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    1);

        }


    }

    public void goMsg(View v) {

        if (ActivityCompat.checkSelfPermission(ContactController.this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {

            Uri uri = Uri.parse("smsto:" + textViewPhone.getText().toString());
            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra("sms_body", "Hello, Word!");
            startActivity(intent);
        } else {
            ActivityCompat.requestPermissions(ContactController.this, new String[]{Manifest.permission.SEND_SMS}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "Permiso aceptado", Toast.LENGTH_SHORT).show();

            }
        }
    }
}