package com.serfinanza.contactbook.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.serfinanza.contactbook.R;
import com.serfinanza.contactbook.adapters.MainAdapter;
import com.serfinanza.contactbook.models.ContactModel;

import java.io.Serializable;
import java.util.ArrayList;

public class MainController extends AppCompatActivity {

    MainAdapter mainAdapter;
    ArrayList<ContactModel> contacts;
    ContactModel model;
    ListView listViewContact;
    FloatingActionButton floatingActionButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewContact = findViewById(R.id.listViewContact);
        floatingActionButton = findViewById(R.id.floatingActionButton);

        contacts = new ArrayList<>();

        model = new ContactModel();
        model.setNames("Daniel Jose");
        model.setSurname("Ruiz Gutierrez");
        model.setPhone("3225285133");
        model.setEmail("djruiz@outlook.es");
        model.setAddress("CRR 72 #54 - 36");
        contacts.add(model);

        model = new ContactModel();
        model.setNames("Jose");
        model.setSurname("Gutierrez");
        model.setPhone("3157275893");
        model.setEmail("jjgutierrez@gmail.es");
        model.setAddress("CRR 98 #25 - 32");
        contacts.add(model);

        model = new ContactModel();
        model.setNames("Carlos");
        model.setSurname("Gamero");
        model.setPhone("3004372431");
        model.setEmail("jcgamero@gmail.es");
        model.setAddress("CRR 85 #75 - 39");
        contacts.add(model);

        mainAdapter = new MainAdapter(MainController.this, contacts);
        listViewContact.setAdapter(mainAdapter);

        getExtras();

        listViewContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ContactModel contactModel = contacts.get(position);
                Intent intent = new Intent(MainController.this, ContactController.class);
                intent.putExtra("names", contactModel.getNames());
                intent.putExtra("surnames", contactModel.getSurname());
                intent.putExtra("phone", contactModel.getPhone());
                intent.putExtra("email", contactModel.getEmail());
                intent.putExtra("address", contactModel.getAddress());
                startActivity(intent);

            }

        });



    }


    public void goRegister(View v) {


        Intent intent = new Intent(MainController.this, RegisterController.class);
        intent.putParcelableArrayListExtra("contacts", contacts);
        startActivity(intent);

    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            contacts = bundle.getParcelableArrayList("contacts");
            System.out.println(contacts.size());
            mainAdapter = new MainAdapter(MainController.this, contacts);
            listViewContact.setAdapter(mainAdapter);
        }

    }


}