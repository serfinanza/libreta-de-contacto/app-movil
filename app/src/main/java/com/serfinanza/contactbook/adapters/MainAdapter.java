package com.serfinanza.contactbook.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.serfinanza.contactbook.R;
import com.serfinanza.contactbook.models.ContactModel;

import java.util.ArrayList;

public class MainAdapter extends BaseAdapter {

    private final Context context;
    private ContactModel model;
    private ArrayList<ContactModel> list;

    public MainAdapter(Context context, ArrayList<ContactModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View itemView = convertView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.item_contact, parent, false);
        }

        TextView textViewItemContact = itemView.findViewById(R.id.textViewItemContact);
        TextView textViewItemMobil = itemView.findViewById(R.id.textViewItemMobil);
        ImageView imageViewItemContact = itemView.findViewById(R.id.imageViewItemContact);

        model = list.get(position);
        String names = model.getNames() + ' ' + model.getSurname();
        textViewItemContact.setText(names);
        textViewItemMobil.setText(model.getPhone());
        imageViewItemContact.setImageResource(R.drawable.ic_avatar_person);

        return itemView;
    }
}
